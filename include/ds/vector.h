
#ifdef CDS_VECTOR_HIGHP
#define CDS_VECTOR_TYPE double
#else
#define CDS_VECTOR_TYPE float
#endif

// ------------- VEC3 BEGIN ----------------------

#ifndef CDS_VEC3
#define CDS_VEC3

struct cds_vec3
{
    CDS_VECTOR_TYPE x;
    CDS_VECTOR_TYPE y;
    CDS_VECTOR_TYPE z;
};

typedef struct cds_vec3 cds_vec3;

cds_vec3 cds_vec3_add(cds_vec3* a, cds_vec3* b);

cds_vec3 cds_vec3_sub(cds_vec3* a, cds_vec3* b);

cds_vec3 cds_vec3_mul(cds_vec3* a, cds_vec3* b);

cds_vec3 cds_vec3_mul_scalar(cds_vec3* a, CDS_VECTOR_TYPE b);

CDS_VECTOR_TYPE cds_vec3_length_squared(cds_vec3* a);

CDS_VECTOR_TYPE cds_vec3_length(cds_vec3* a);

cds_vec3 cds_vec3_normalized(cds_vec3* a);

cds_vec3* cds_vec3_normalize(cds_vec3* a);

cds_vec3 cds_vec3_random();

cds_vec3 cds_vec3_random_in_range(CDS_VECTOR_TYPE min, CDS_VECTOR_TYPE max);

CDS_VECTOR_TYPE cds_vec3_get_axis(cds_vec3* a, int axis);

cds_vec3 cds_vec3_corss(cds_vec3* a, cds_vec3* b);

CDS_VECTOR_TYPE cds_vec3_dot(cds_vec3* a, cds_vec3* b);

#ifdef CDS_IMPLEMENTATION

#include <stdlib.h>
#include <math.h>

cds_vec3 cds_vec3_add(cds_vec3* a, cds_vec3* b)
{
    cds_vec3 v;
    v.x = a->x + b->x;
    v.y = a->y + b->y;
    v.z = a->z + b->z;
    return v;
}

cds_vec3 cds_vec3_sub(cds_vec3* a, cds_vec3* b)
{
    cds_vec3 v;
    v.x = a->x - b->x;
    v.y = a->y - b->y;
    v.z = a->z - b->z;
    return v;
}

cds_vec3 cds_vec3_mul(cds_vec3* a, cds_vec3* b)
{
    cds_vec3 v;
    v.x = a->x * b->x;
    v.y = a->y * b->y;
    v.z = a->z * b->z;
    return v;
}

cds_vec3 cds_vec3_mul_scalar(cds_vec3* a, CDS_VECTOR_TYPE b)
{
    cds_vec3 v;
    v.x = a->x * b;
    v.y = a->y * b;
    v.z = a->z * b;
    return v;
}

CDS_VECTOR_TYPE cds_vec3_length_squared(cds_vec3* a)
{
    return (a->x * a->x) + (a->y * a->y) + (a->z * a->z);
}

CDS_VECTOR_TYPE cds_vec3_length(cds_vec3* a)
{
    return (CDS_VECTOR_TYPE)sqrt((a->x * a->x) + (a->y * a->y) + (a->z * a->z));
}

cds_vec3 cds_vec3_normalized(cds_vec3* a)
{
    CDS_VECTOR_TYPE length = (CDS_VECTOR_TYPE)sqrt((a->x * a->x) + (a->y * a->y) + (a->z * a->z));
    cds_vec3 v;
    v.x = a->x / length;
    v.y = a->y / length;
    v.z = a->z / length;
    return v;
}

cds_vec3* cds_vec3_normalize(cds_vec3* a)
{
    CDS_VECTOR_TYPE length = (CDS_VECTOR_TYPE)sqrt((a->x * a->x) + (a->y * a->y) + (a->z * a->z));
    a->x /= length;
    a->y /= length;
    a->z /= length;
    return a;
}

cds_vec3 cds_vec3_random()
{
    cds_vec3 v;
    v.x = (CDS_VECTOR_TYPE)rand() / RAND_MAX;
    v.y = (CDS_VECTOR_TYPE)rand() / RAND_MAX;
    v.z = (CDS_VECTOR_TYPE)rand() / RAND_MAX;
    return v;
}

cds_vec3 cds_vec3_random_in_range(CDS_VECTOR_TYPE min, CDS_VECTOR_TYPE max)
{
    cds_vec3 v;
    v.x = ((CDS_VECTOR_TYPE)rand() / RAND_MAX) * (max - min) + min;
    v.y = ((CDS_VECTOR_TYPE)rand() / RAND_MAX) * (max - min) + min;
    v.z = ((CDS_VECTOR_TYPE)rand() / RAND_MAX) * (max - min) + min;
    return v;
}

CDS_VECTOR_TYPE cds_vec3_get_axis(cds_vec3* a, int axis)
{
    switch(axis)
    {
        case 0:   return a->x;
        case 1:   return a->y;
        case 2:   return a->z;
        default:  return 0;
    }
}

cds_vec3 cds_vec3_corss(cds_vec3* a, cds_vec3* b)
{
    cds_vec3 v;
    v.x = a->y * b->z - a->z * b->y;
    v.y = a->z * b->x - a->x * b->z;
    v.z = a->x * b->y - a->y * b->x;
    return v;
}

CDS_VECTOR_TYPE cds_vec3_dot(cds_vec3* a, cds_vec3* b)
{
    return a->x * b->x + a->y * b->y + a->z * b->z;
}


#endif // CDS_IMPLEMENTATION

#endif

// ------------- VEC3 END ------------------------


// ------------- VEC2 BEGIN ----------------------

#ifndef CDS_VEC2
#define CDS_VEC2

struct cds_vec2
{
    CDS_VECTOR_TYPE x;
    CDS_VECTOR_TYPE y;
};

typedef struct cds_vec2 cds_vec2;

cds_vec2 cds_vec2_add(cds_vec2* a, cds_vec2* b);

cds_vec2 cds_vec2_sub(cds_vec2* a, cds_vec2* b);

cds_vec2 cds_vec2_mul(cds_vec2* a, cds_vec2* b);

cds_vec2 cds_vec2_mul_scalar(cds_vec2* a, CDS_VECTOR_TYPE b);

CDS_VECTOR_TYPE cds_vec2_length_squared(cds_vec2* a);

CDS_VECTOR_TYPE cds_vec2_length(cds_vec2* a);

cds_vec2 cds_vec2_normalized(cds_vec2* a);

cds_vec2* cds_vec2_normalize(cds_vec2* a);

cds_vec2 cds_vec2_random();

cds_vec2 cds_vec2_random_in_range(CDS_VECTOR_TYPE min, CDS_VECTOR_TYPE max);

CDS_VECTOR_TYPE cds_vec2_get_axis(cds_vec2* a, int axis);

CDS_VECTOR_TYPE cds_vec2_corss(cds_vec2* a, cds_vec2* b);

CDS_VECTOR_TYPE cds_vec2_dot(cds_vec2* a, cds_vec2* b);

#ifdef CDS_IMPLEMENTATION

#include <stdlib.h>
#include <math.h>

cds_vec2 cds_vec2_add(cds_vec2* a, cds_vec2* b)
{
    cds_vec2 v;
    v.x = a->x + b->x;
    v.y = a->y + b->y;
    return v;
}

cds_vec2 cds_vec2_sub(cds_vec2* a, cds_vec2* b)
{
    cds_vec2 v;
    v.x = a->x - b->x;
    v.y = a->y - b->y;
    return v;
}

cds_vec2 cds_vec2_mul(cds_vec2* a, cds_vec2* b)
{
    cds_vec2 v;
    v.x = a->x * b->x;
    v.y = a->y * b->y;
    return v;
}

cds_vec2 cds_vec2_mul_scalar(cds_vec2* a, CDS_VECTOR_TYPE b)
{
    cds_vec2 v;
    v.x = a->x * b;
    v.y = a->y * b;
    return v;
}

CDS_VECTOR_TYPE cds_vec2_length_squared(cds_vec2* a)
{
    return (a->x * a->x) + (a->y * a->y);
}

CDS_VECTOR_TYPE cds_vec2_length(cds_vec2* a)
{
    return (CDS_VECTOR_TYPE)sqrt((a->x * a->x) + (a->y * a->y));
}

cds_vec2 cds_vec2_normalized(cds_vec2* a)
{
    CDS_VECTOR_TYPE length = (CDS_VECTOR_TYPE)sqrt((a->x * a->x) + (a->y * a->y));
    cds_vec2 v;
    v.x = a->x / length;
    v.y = a->y / length;
}

cds_vec2* cds_vec2_normalize(cds_vec2* a)
{
    CDS_VECTOR_TYPE length = (CDS_VECTOR_TYPE)sqrt((a->x * a->x) + (a->y * a->y));
    a->x /= length;
    a->y /= length;
    return a;
}

cds_vec2 cds_vec2_random()
{
    cds_vec2 v;
    v.x = (CDS_VECTOR_TYPE)rand() / RAND_MAX;
    v.y = (CDS_VECTOR_TYPE)rand() / RAND_MAX;
    return v;
}

cds_vec2 cds_vec2_random_in_range(CDS_VECTOR_TYPE min, CDS_VECTOR_TYPE max)
{
    cds_vec2 v;
    v.x = ((CDS_VECTOR_TYPE)rand() / RAND_MAX) * (max - min) + min;
    v.y = ((CDS_VECTOR_TYPE)rand() / RAND_MAX) * (max - min) + min;
    return v;
}

CDS_VECTOR_TYPE cds_vec2_get_axis(cds_vec2* a, int axis)
{
    switch(axis)
    {
        case 0:   return a->x;
        case 1:   return a->y;
        default:  return 0;
    }
}

CDS_VECTOR_TYPE cds_vec2_corss(cds_vec2* a, cds_vec2* b)
{
    return a->x * b->y - a->y * b->x;
}

CDS_VECTOR_TYPE cds_vec2_dot(cds_vec2* a, cds_vec2* b)
{
    return a->x * b->x + a->y * b->y;
}


#endif // CDS_IMPLEMENTATION

#endif

// ------------- VEC2 END ------------------------