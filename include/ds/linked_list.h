#ifndef CDS_LINKED_LIST
#define CDS_LINKED_LIST

struct cds_linked_list_node
{ 
    void* value; 
    struct cds_linked_list_node* next;
}; 

typedef struct cds_linked_list_node cds_linked_list_node;

cds_linked_list_node* cds_linked_list_node_allocate();

void cds_linked_list_node_free(cds_linked_list_node* node);

void cds_linked_list_free(cds_linked_list_node* top);

void cds_linked_list_add_node(cds_linked_list_node* node, void* value);

#ifdef CDS_IMPLEMENTATION

#include <stdlib.h>

cds_linked_list_node* cds_linked_list_node_allocate()
{
    cds_linked_list_node* node = (cds_linked_list_node*)malloc(sizeof(cds_linked_list_node));
    node->value = NULL;
    node->next = NULL;
    return node;
}

void cds_linked_list_node_free(cds_linked_list_node* node)
{
    free(node);
}

void cds_linked_list_free(cds_linked_list_node* top)
{
    cds_linked_list_node* tmp;
    while(top->next != NULL)
    {
        tmp = top;
        free(tmp);
        top = top->next;
    }
}

void cds_linked_list_add_node(cds_linked_list_node* node, void* value)
{
    cds_linked_list_node* nd = cds_linked_list_node_allocate();
    nd->value = value;
    if(node->next != NULL)
        nd->next = node->next;
    node->next = nd;
}


#endif // CDS_IMPLEMENTATION

#endif // CDS_LINKED_LIST