#ifndef CDS_DYNAMIC_ARRAY
#define CDS_DYNAMIC_ARRAY

struct cds_dynamic_array
{
    int size;
    int count;
    int capacity;
    float increment_factor;
    void* data;
};

typedef struct cds_dynamic_array cds_dynamic_array;

// Allocates a dynamic array
cds_dynamic_array* cds_dynamic_array_allocate(int size, int initial_capacity);

// Frees the dynamic array
void cds_dynamic_array_free(cds_dynamic_array* array);

void cds_dynamic_array_set(cds_dynamic_array* array, int index, void* data);

void* cds_dynamic_array_get(cds_dynamic_array* array, int index, void* data);

void cds_dynamic_array_resize(cds_dynamic_array* array, int new_capacity);

#ifdef CDS_IMPLEMENTATION

#include <stdlib.h>
#include <string.h>

#ifdef _DEBUG
#include <stdio.h>
#endif

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))


cds_dynamic_array* cds_dynamic_array_allocate(int size, int initial_capacity)
{
    cds_dynamic_array* array = (cds_dynamic_array*)malloc(sizeof(cds_dynamic_array));
    array->count = 0;
    array->capacity = initial_capacity;
    array->size = size;
    array->data = malloc(array->size * array->capacity);
    array->count = 0;
    array->increment_factor = 1.5f;
    return array;
}

void cds_dynamic_array_free(cds_dynamic_array* array)
{
    free(array->data);
    free(array);
}

void cds_dynamic_array_set(cds_dynamic_array* array, int index, void* data)
{
    #ifdef _DEBUG
    if(index < 0)
        printf("Negetive Index Error\n");
    #endif
    if(index > array->capacity)
    {
        cds_dynamic_array_resize(array, (int)(array->capacity * array->increment_factor));
    }
    memcpy(array->data + array->size * index, data, array->size);
    array->count = MAX(index + 1, array->count);
}

void* cds_dynamic_array_get(cds_dynamic_array* array, int index, void* data)
{
    #ifdef _DEBUG
    if(index < 0)
        printf("Negetive Index Error\n");
    #endif
    if(index > array->capacity)
    {
        cds_dynamic_array_resize(array, (int)(array->capacity * array->increment_factor));
    }
    memcpy(data, array->data + array->size * index, array->size);
    array->count = MAX(index + 1, array->count);
    return data;
}

void cds_dynamic_array_resize(cds_dynamic_array* array, int new_capacity)
{
    array->data = realloc(array->data, new_capacity*array->size);
    array->capacity = new_capacity;
}

#endif // CDS_IMPLEMENTATION

#endif // CDS_DYNAMIC_ARRAY