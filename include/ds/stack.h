#ifndef CDS_STACK
#define CDS_STACK

#ifndef CDS_STACK_INITIAL_CAPACITY
#define CDS_STACK_INITIAL_CAPACITY 10
#endif

#ifndef CDS_STACK_USE_ARRAY
#define CDS_STACK_USE_LINKED_LIST
#endif

struct cds_stack
{
    int size;
    int capacity;
    int count;
    void* top;
};

typedef struct cds_stack cds_stack;

// Allocates a stack
cds_stack* cds_stack_allocate(int size);

// Frees the stack
void cds_stack_free(cds_stack* stack);

void cds_stack_push(cds_stack* stack, void* data);

void* cds_stack_peek(cds_stack* stack, void* data);

void* cds_stack_pop(cds_stack* stack, void* data);

void cds_stack_empty(cds_stack* stack);

#ifdef CDS_IMPLEMENTATION

#include <stdlib.h>
#include <string.h>

#ifdef _DEBUG
#include <stdio.h>
#endif

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

#ifdef CDS_STACK_USE_ARRAY

static void cds_stack_resize(cds_stack* stack, int new_capacity)
{
    stack->top = realloc(stack->top, new_capacity * stack->size);
    stack->capacity = new_capacity;
}

// Allocates a stack
cds_stack* cds_stack_allocate(int size)
{
    cds_stack* stack = (cds_stack*)malloc(sizeof(cds_stack));
    stack->size = size;
    stack->capacity = CDS_STACK_INITIAL_CAPACITY;
    stack->count = 0;
    stack->top = malloc(size * CDS_STACK_INITIAL_CAPACITY);
    return stack;
}

// Frees the stack
void cds_stack_free(cds_stack* stack)
{
    free(stack->top);
    free(stack);
}

void cds_stack_push(cds_stack* stack, void* data)
{
    if(stack->count == stack->capacity)
    {
        cds_stack_resize(stack, stack->capacity * 2);
    }
    memcpy(stack->top + stack->count * stack->size, data, stack->size);
    stack->count += 1;
}

void* cds_stack_peek(cds_stack* stack, void* data)
{
    memcpy(data, stack->top + (stack->count-1) * stack->size, stack->size);
    return data;

}
void* cds_stack_pop(cds_stack* stack, void* data)
{
    stack->count -= 1;
    memcpy(data, stack->top + stack->count * stack->size, stack->size);
    return data;
}

void cds_stack_empty(cds_stack* stack)
{
    stack->count = 0;
}


#endif

#ifdef CDS_STACK_USE_LINKED_LIST

struct cds_stack_node
{
    void* value;
    struct cds_stack_node* next;
};

typedef struct cds_stack_node cds_stack_node;

// Allocates a stack
cds_stack* cds_stack_allocate(int size)
{
    cds_stack* stack = (cds_stack*)malloc(sizeof(cds_stack));
    stack->size = size;
    stack->capacity = -1;
    stack->count = 0;
    stack->top = NULL;
    return stack;
}

// Frees the stack
void cds_stack_free(cds_stack* stack)
{
    cds_stack_empty(stack);
    free(stack);
}

void cds_stack_push(cds_stack* stack, void* data)
{
    stack->count += 1;
    cds_stack_node* node = (cds_stack_node*)malloc(sizeof(cds_stack_node));
    node->next = stack->top;
    node->value = malloc(stack->size);
    memcpy(node->value, data, stack->size);
    stack->top = node;
}

void* cds_stack_peek(cds_stack* stack, void* data)
{
    memcpy(data, ((cds_stack_node*)stack->top)->value, stack->size);
    return data;
}

void* cds_stack_pop(cds_stack* stack, void* data)
{
    stack->count -= 1;
    memcpy(data, ((cds_stack_node*)stack->top)->value, stack->size);
    cds_stack_node* tmp = ((cds_stack_node*)stack->top)->next;
    free(((cds_stack_node*)stack->top)->value);
    free(stack->top);
    stack->top = tmp;
    return data;
}

void cds_stack_empty(cds_stack* stack)
{
    cds_stack_node* top_node = stack->top;
    cds_stack_node* tmp;

    while(top_node != NULL)
    {
        tmp = top_node;
        free(tmp->value);
        free(tmp);
        top_node = top_node->next;
    }
    stack->count = 0;
}

#endif


#endif // CDS_IMPLEMENTATION

#endif // CDS_STACK