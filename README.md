# cds

Data Structures And Algorithms in C

<!-- Badges -->

<!-- Visuals -->


## Installation

To clone:

    git clone https://gitlab.com/Jaysmito101/cds.git
    cd cds

<!-- Usage -->

## What Data Structures Are Available?

| Name                    | Headers              | Description                      |
|-------------------------|----------------------|----------------------------------|
| Dynamic Array           | [dynamic_array.h](./include/ds/dynamic_array.h)| This Provides a easy, simple and fast <br> dynamically managed arrays.  |
| Array Based Stack | [stack.h](./include/ds/stack.h) | Provides a fast stack implemented using an array. |
| Linked List Based Stack | [stack.h](./include/ds/stack.h) | Provides a fast stack implemented using an Linked List. |
| Vector 3 | [vector.h](./include/ds/vector.h) | 3D Vector with all accompanying functions needed! |
| Vector 2 | [vector.h](./include/ds/vector.h) | 2D Vector with all accompanying functions needed! |
| Linked List | [linked_list.h](./include/ds/linked_list.h) | Simple easy Linked List |


## Support
Any support is highly appretiated.

## License

    MIT License
    
    Copyright (c) 2022 Jaysmito Mukherjee (jaysmito101@gmail.com)
    
    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:
    
    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.
    
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
