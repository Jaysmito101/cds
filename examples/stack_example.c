#include <stdio.h>
// #define CDS_STACK_USE_ARRAY Optional
#define CDS_IMPLEMENTATION
#include "cds.h"

int main()
{
    cds_stack* stack = cds_stack_allocate(sizeof(int));
    
    int tmp = 1;

    printf("Setting Values ...\n");
    for(int i = 0;i<20;i++)
    {
        tmp = i*i;
        cds_stack_push(stack, &tmp);
    }


    printf("Getting Values ...\n");
    while(stack->count > 0)
    {
        cds_stack_pop(stack, &tmp);
        printf("Element Popped : %d\n", tmp);
    }

    cds_stack_free(stack);
    
    return 0;
}