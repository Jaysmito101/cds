#include <stdio.h>

#define CDS_IMPLEMENTATION
#include "ds/dynamic_array.h"

int main()
{
    cds_dynamic_array* arr = cds_dynamic_array_allocate(sizeof(int), 5);
    
    int tmp = 1;

    printf("--------------\nArray Item Count : %d\nArray Capacity: %d\n--------------\n", arr->count, arr->capacity);

    printf("Setting Values ...\n");
    for(int i = 0;i<10;i++)
    {
        tmp = i*i;
        cds_dynamic_array_set(arr, i, &tmp);
    }

    printf("--------------\nArray Item Count : %d\nArray Capacity: %d\n--------------\n", arr->count, arr->capacity);

    printf("Getting Values ...\n--------------\n");
    for(int i = 0;i<arr->count;i++)
    {
        cds_dynamic_array_get(arr, i, &tmp);
        printf("Element %d : %d\n", i, tmp);
    }
    printf("--------------\nDone!\n--------------\n");
    cds_dynamic_array_free(arr);
    return 0;
}