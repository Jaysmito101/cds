#define CDS_STACK_USE_ARRAY
#define CDS_IMPLEMENTATION
#include "cds.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
    srand(time(0));
    // VEC2

    cds_vec3 a, b, c;
    a.x = 0;
    a.y = 1;
    a.z = 2;

    c = cds_vec3_random();

    b = cds_vec3_normalized(&a);

    printf("Original   : ( X : %f, Y : %f, Z : %f )\n", a.x, a.y, a.z);
    printf("Normalized : ( X : %f, Y : %f, Z : %f )\n", b.x, b.y, b.z);
    printf("Random     : ( X : %f, Y : %f, Z : %f )\n", c.x, c.y, c.z);
    

 
    
    return 0;
}